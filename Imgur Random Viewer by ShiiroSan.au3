#NoTrayIcon
#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_Compression=4
#AutoIt3Wrapper_UseUpx=y
#AutoIt3Wrapper_UseX64=n
#AutoIt3Wrapper_Res_Comment=Software made to view image on Imgur with amazing random tools and other few things like that. Ofc this is purely useless and... Wait why are you reading this?!
#AutoIt3Wrapper_Res_Description=Software made to view image on Imgur (with amazing random tools)
#AutoIt3Wrapper_Res_Fileversion=0.2
#AutoIt3Wrapper_Res_LegalCopyright=ShiiroSan
#AutoIt3Wrapper_Res_Language=1036
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****

$ServerURL = "https://www.shiirosan.com/sw/irv/"

$French = 0
$English = 0
$Silent = 0
$Dev = 0
$Tray = 0

For $i = 1 To $Cmdline[0] Step 1
	If $Cmdline[$i] = "-french" Then $French = 1
	If $Cmdline[$i] = "-english" Then $English = 1
	If $Cmdline[$i] = "-silent" Then $Silent = 1
	If $Cmdline[$i] = "-dev" Then $Dev = 1
	If $Cmdline[$i] = "-tray" Then $Tray = 1
Next

If $French And $English Then
	MsgBox(0, "ERROR!", "Error with parameters! Only one language are usable at time!")
	Exit 1
EndIf

If FileExists(@AppDataDir & "\IMGURViewer") <> 1 Then
	Local $folder = DirCreate(@AppDataDir & "\IMGURViewer\LanguagesFiles\")
	If $folder <> 1 Then
		MsgBox(0, "ERROR!", "Please run the program on admin once. Exiting...")
		Exit 2
	EndIf
EndIf
If FileExists(@AppDataDir & "\IMGURViewer\LanguagesFiles") <> 1 Then
	Local $folder = DirCreate(@AppDataDir & "\IMGURViewer\LanguagesFiles\")
	If $folder <> 1 Then
		MsgBox(0, "ERROR!", "Please run the program on admin once. Exiting...")
		Exit 2
	EndIf
EndIf
If FileExists(@AppDataDir & "\IMGURViewer\LanguagesFiles\Languages.cfg") <> 1 Then
	InetGet($ServerURL & "ENGLISH", @AppDataDir & "\IMGURViewer\LanguagesFiles\ENGLISH.EUX", 1, 0)
	InetGet($ServerURL & "FRENCH", @AppDataDir & "\IMGURViewer\LanguagesFiles\FRENCH.EUX", 1, 0)
	InetGet($ServerURL & "Languages.cfg", @AppDataDir & "\IMGURViewer\LanguagesFiles\Languages.cfg", 1, 0)
EndIf
If  Not FileExists(@AppDataDir & "\IMGURViewer\Config.dat") Then ;A changer pour modifier le fichier � chaque d�marrage
	IniWrite(@AppDataDir & "\IMGURViewer\Config.dat", "Param", "Timer", "10")
	IniWrite(@AppDataDir & "\IMGURViewer\Config.dat", "Param", "Savefolder", @UserProfileDir & "\Pictures")
	IniWrite(@AppDataDir & "\IMGURViewer\Config.dat", "Param", "WhatsNew", "0")
	IniWrite(@AppDataDir & "\IMGURViewer\Config.dat", "Param", "Language", "0409")
	IniWrite(@AppDataDir & "\IMGURViewer\Config.dat", "Param", "SilentMode", "0")
	IniWrite(@AppDataDir & "\IMGURViewer\Config.dat", "Param", "SilentThumbTime", "3000")
	IniWrite(@AppDataDir & "\IMGURViewer\Config.dat", "Default", "DTimer", "10")
	IniWrite(@AppDataDir & "\IMGURViewer\Config.dat", "Default", "DSavefolder", @UserProfileDir & "\Pictures")
	IniWrite(@AppDataDir & "\IMGURViewer\Config.dat", "Default", "DWhatsNew", "0")
	IniWrite(@AppDataDir & "\IMGURViewer\Config.dat", "Default", "DLanguage", "0409")
	IniWrite(@AppDataDir & "\IMGURViewer\Config.dat", "Default", "SilentMode", "0")
	IniWrite(@AppDataDir & "\IMGURViewer\Config.dat", "Default", "SilentThumbTime", "3000")
	IniWrite(@AppDataDir & "\IMGURViewer\Config.dat", "Version", "Actual", "DevRelease")
Else
	If IniRead(@AppDataDir & "\IMGURViewer\Config.dat", "Version", "Actual", "-1") <> "0.2.5" Then
		FileDelete(@AppDataDir & "\IMGURViewer\Config.dat")
		ShellExecute(@ScriptDir & "\" & @ScriptName)
		Exit 1
	EndIf
EndIf
$VersionNumber = IniRead(@AppDataDir & "\IMGURViewer\Config.dat", "Version", "Actual", "-1")
#include <ButtonConstants.au3>
#include <EditConstants.au3>
#include <GUIConstantsEx.au3>
#include <ProgressConstants.au3>
#include <StaticConstants.au3>
#include <TabConstants.au3>
#include <WindowsConstants.au3>
#include <StringConstants.au3>
#include <GUIListBox.au3>
#include <IE.au3>
#include <_SelfDelete.au3>
#include <INet.au3>
#Region ### START Koda GUI section ### Form=

#include "MultiLang.au3"
#include "MD5.au3"

Local $LANG_DIR = @AppDataDir & "\IMGURViewer\LanguagesFiles" ; Where we are storing the language files.
Local $user_lang = -1
;~ Local $LANGFILES[IniRead($LANG_DIR&"\Languages.cfg","Cfg","First","")][IniRead($LANG_DIR&"\Languages.cfg","Cfg","Second","")]
Local $LANGFILES[2][3]
$LANGFILES[0][0] = IniRead($LANG_DIR & "\Languages.cfg", "Languages", "00", "")
$LANGFILES[0][1] = $LANG_DIR & "\ENGLISH.EUX"
$LANGFILES[0][2] = IniRead($LANG_DIR & "\Languages.cfg", "Languages", "02", "")

$LANGFILES[1][0] = IniRead($LANG_DIR & "\Languages.cfg", "Languages", "10", "")
$LANGFILES[1][1] = $LANG_DIR & "\FRENCH.EUX"
$LANGFILES[1][2] = IniRead($LANG_DIR & "\Languages.cfg", "Languages", "12", "")

_MultiLang_SetFileInfo($LANGFILES)
If @error Then
	MsgBox(48, "Error", "Could not set file info.  Error Code " & @error)
	Exit
EndIf
$user_lang = IniRead(@AppDataDir & "\IMGURViewer\Config.dat", "Param", "Language", "0409")
$ret = _MultiLang_LoadLangFile($user_lang)
If @error Then
	MsgBox(48, "Error", "Could not load lang file.  Error Code " & @error)
	Exit
EndIf

If $ret = 2 Then
	MsgBox(64, "Information", "Just letting you know that we loaded the default language file")
EndIf

$Buttonpressed = 0
;~ Update()

Global $Form1 = GUICreate(_MultiLang_GetText("main_gui") & IniRead(@AppDataDir & "\IMGURViewer\Config.dat", "Version", "Actual", "-1"), 1366, 700, 70, 49, BitOR($GUI_SS_DEFAULT_GUI, $WS_TABSTOP))
$oIE = ObjCreate("Shell.Explorer.2")
$oIE2 = _IECreate("", 0, 0, 1)
$oIE3 = ObjCreate("Shell.Explorer.2")
Global $Tab1 = GUICtrlCreateTab(-1, -1, 1366, 700)
Global $TabSheet1 = GUICtrlCreateTabItem(_MultiLang_GetText("viewer"))
$GUIActiveX = GUICtrlCreateObj($oIE, 3, 30, 1366 - 9, 700 - 150)
Global $btnOpenInBrowserMainGUI = GUICtrlCreateButton(_MultiLang_GetText("browser"), 207, 592, 77, 25)
Global $Random = GUICtrlCreateButton(_MultiLang_GetText("random"), 680, 600, 675, 49)
Global $Button3 = GUICtrlCreateButton(_MultiLang_GetText("button3"), 375, 656, 140, 25)
Global $btnBlackListMainGUI = GUICtrlCreateButton(_MultiLang_GetText("block1"), 375, 624, 100, 25)
Global $btnFavMainGUI = GUICtrlCreateButton(_MultiLang_GetText("fav1"), 375, 592, 100, 25)
Global $btnDlMainGUI = GUICtrlCreateButton(_MultiLang_GetText("download"), 480, 600, 195, 49)
Global $btnCpLink1MainGUI = GUICtrlCreateButton(_MultiLang_GetText("copylink"), 286, 592, 77, 25)
Global $btnCpLink2MainGUI = GUICtrlCreateButton(_MultiLang_GetText("copylink"), 286, 624, 77, 25)
Global $btnCpLink3MainGUI = GUICtrlCreateButton(_MultiLang_GetText("copylink"), 286, 656, 77, 25)
Global $Input1 = GUICtrlCreateInput("", 65, 624, 215, 25)
Global $Input2 = GUICtrlCreateInput("", 65, 656, 215, 25)
Global $Input3 = GUICtrlCreateInput("", 65, 592, 137, 25)
Global $Progress1 = GUICtrlCreateProgress(728, 656, 622, 33)
Global $Checkbox1 = GUICtrlCreateCheckbox(_MultiLang_GetText("autrand") & " " & ConfigRead("Timer") & " secondes.", 520, 656, 170, 25)
Global $Label1 = GUICtrlCreateLabel("Miniature : ", 8, 659, 50, 30)
Global $Label3 = GUICtrlCreateLabel(_MultiLang_GetText("link"), 8, 599, 50, 30)
Global $Label4 = GUICtrlCreateLabel("Forum : ", 8, 629, 50, 30)
Global $TabSheet2 = GUICtrlCreateTabItem(_MultiLang_GetText("history"))
Global $List1 = GUICtrlCreateList("", 8, 30, 600, 665, BitOR($LBS_NOTIFY, $WS_VSCROLL, $WS_BORDER))
GUICtrlSetData(-1, FileRead(@AppDataDir & "\IMGURViewer\History.txt"))
$GUIActiveX2 = GUICtrlCreateObj($oIE3, 615, 30, 740, 500)
Global $btnLoadIMGHistGUI = GUICtrlCreateButton(_MultiLang_GetText("load"), 625, 535, 115, 60)
Global $HiddenInput = GUICtrlCreateInput("", 500, 500, 1, 1)
GUICtrlSetState(-1, $GUI_HIDE)
Global $btnCpLinkHistGUI = GUICtrlCreateButton(_MultiLang_GetText("copylink"), 745, 535, 115, 60)
Global $btnDlHistory = GUICtrlCreateButton(_MultiLang_GetText("download"), 865, 535, 115, 60)
Global $btnFavHistGUI = GUICtrlCreateButton(_MultiLang_GetText("fav1"), 985, 535, 115, 60)
Global $btnBlacklistHistGUI = GUICtrlCreateButton(_MultiLang_GetText("blacklist"), 1105, 535, 115, 60)
Global $btnClearHistGUI = GUICtrlCreateButton(_MultiLang_GetText("cleanhistory"), 1225, 535, 115, 60)
Global $TabSheet3 = GUICtrlCreateTabItem(_MultiLang_GetText("fav2"))
Global $TabSheet5 = GUICtrlCreateTabItem(_MultiLang_GetText("banned"))
Global $TabSheet4 = GUICtrlCreateTabItem(_MultiLang_GetText("option"))
Global $Label5 = GUICtrlCreateLabel(_MultiLang_GetText("tbswtch") & " (1-90 sec) : ", 3, 50, 200, 20)
Global $Input4 = GUICtrlCreateInput(IniRead(@AppDataDir & "\IMGURViewer\Config.dat", "Param", "Timer", "10"), 185, 50, 50, 17, $ES_NUMBER)
Global $Label6 = GUICtrlCreateLabel(_MultiLang_GetText("foldersave") & " : ", 3, 70, 200, 20)
Global $Input5 = GUICtrlCreateInput(IniRead(@AppDataDir & "\IMGURViewer\Config.dat", "Param", "Savefolder", @UserProfileDir & "\Pictures"), 185, 70, 160, 17, $ES_READONLY)
Global $btnSelectFolderParamGUI = GUICtrlCreateButton("...", 347, 70, 20, 17)
Global $Label9 = GUICtrlCreateLabel(_MultiLang_GetText("lang"), 3, 115)
Global $Combo = GUICtrlCreateCombo("(Select A Language)", 55, 112, 115, 25, 0x0003)
GUICtrlSetData(-1, $LANGFILES[0][0] & "|" & $LANGFILES[1][0], "(Select A Language)")
Global $Label10 = GUICtrlCreateLabel("Silent Mode: ", 3, 140)
GUICtrlSetTip(-1, "Can be controled on tray icon or with hotkey.")
Global $Checkbox4 = GUICtrlCreateCheckbox("", 80, 139, 17, 17)
Global $Label11 = GUICtrlCreateLabel("Time for thumbnail view (in seconds): ", 3, 162)
Global $Input6 = GUICtrlCreateInput(IniRead(@AppDataDir & "\IMGURViewer\Config.dat", "Param", "SilentThumbTime", "3") / 1000, 185, 160, 50, 17, $ES_NUMBER)
GUICtrlSetState(-1, $GUI_DISABLE)
Global $Label7 = GUICtrlCreateLabel("Version: ", 5, 610, 45, 20)
Global $Label8 = GUICtrlCreateLabel(IniRead(@AppDataDir & "\IMGURViewer\Config.dat", "Version", "Actual", "-1"), 50, 610, 40, 20)
Global $btnUpdateParamGUI = GUICtrlCreateButton(_MultiLang_GetText("chkupd"), 80, 607, 120, 20)
Global $Save = GUICtrlCreateButton(_MultiLang_GetText("savecfg"), 980, 630, 375, 60)
Global $Default = GUICtrlCreateButton(_MultiLang_GetText("defaultcfg"), 5, 630, 100, 60)
Global $Checkbox3 = GUICtrlCreateCheckbox(_MultiLang_GetText("showchangelogs"), 3, 90, 185, 17)
If IniRead(@AppDataDir & "\IMGURViewer\Config.dat", "Param", "WhatsNew", 0) = 0 Then
	GUICtrlSetState($Checkbox3, $GUI_CHECKED)
Else
	GUICtrlSetState($Checkbox3, $GUI_UNCHECKED)
EndIf
$idLoad = TrayCreateItem("Load an image")
TrayCreateItem("")
$idShowThumb = TrayCreateItem("Show the image as thumbnail")
$idDL = TrayCreateItem("Download current image")
$idOpen = TrayCreateItem("Open the image on browser")
$idShare = TrayCreateItem("Get link of current image")
TrayCreateItem("")
$idShowGUI = TrayCreateItem("Show GUI")
TrayCreateItem("")
$idQuit = TrayCreateItem("Quit")
If IniRead(@AppDataDir & "\IMGURViewer\Config.dat", "Param", "SilentMode", 0) = 1 Then
	GUICtrlSetState($Input6, $GUI_ENABLE)
	GUICtrlSetState($Checkbox4, $GUI_CHECKED)
	GUISetState(@SW_HIDE, $Form1)
	TraySetState(1)
	Opt("TrayMenuMode", 3)

	Global $Silent = GUICreate("", 325, 325, 351, 174, 0)
	GUISetState(@SW_HIDE)
	$sIE = ObjCreate("Shell.Explorer.2")
	$sIEActiveX = GUICtrlCreateObj($sIE, -1, -1, 325, 325)

Else
	GUISetState(@SW_SHOW)
	GUICtrlSetState($Checkbox4, $GUI_UNCHECKED)
EndIf
#EndRegion ### END Koda GUI section ###
InetGet("https://www.shiirosan.com/sw/irv/changelog", @TempDir & "\new.txt")
Global $About = GUICreate(_MultiLang_GetText("whatnew"), 358, 392, -1, -1, BitXOR($GUI_SS_DEFAULT_GUI, $WS_SIZEBOX, $WS_MINIMIZEBOX), -1, WinGetHandle(AutoItWinGetTitle()))
Global $Label7 = GUICtrlCreateLabel(_MultiLang_GetText("changelogs"), 0, 6, 114, 24)
GUICtrlSetFont(-1, 12, 800, 0, "MS Sans Serif")
Global $Edit1 = GUICtrlCreateEdit("", 0, 32, 353, 321, BitOR($ES_AUTOVSCROLL, $ES_AUTOHSCROLL, $ES_WANTRETURN, $WS_VSCROLL, $ES_READONLY))
GUICtrlSetData(-1, FileRead(@TempDir & "\new.txt"))
Global $Checkbox2 = GUICtrlCreateCheckbox(_MultiLang_GetText("dontshownext"), 8, 368, 129, 17)
Global $btnCloseChangeLogGUI = GUICtrlCreateButton(_MultiLang_GetText("close"), 216, 360, 131, 25)
$Exit = 0
If IniRead(@AppDataDir & "\IMGURViewer\Config.dat", "Param", "WhatsNew", 1) = 0 Then
	GUISetState(@SW_SHOW)
Else
	GUISetState(@SW_HIDE)
	$Exit = 1
EndIf
$oIE.navigate("https://steamcommunity.com/profiles/76561197971703525")
$oIE3.navigate("https://steamcommunity.com/profiles/76561197971703525")
While 1
	$nMsg = GUIGetMsg()
	If IniRead(@AppDataDir & "\IMGURViewer\Config.dat", "Param", "SilentMode", 0) = 1 Then
		Switch TrayGetMsg()
			Case $idQuit
				$oIE.quit()
				$sIE.quit()
				$oIE3.quit()
				_IEQuit($oIE2)
				GUIDelete()
				Exit
			Case $idLoad
				RandomURL()
				TraySetState(4)
			Case $idShowGUI
				If TrayItemGetState($idShowGUI) = 68 Then
					TrayItemSetState($idShowGUI, $GUI_CHECKED)
					GUISetState(@SW_SHOW, $Form1)
				Else
					TrayItemSetState($idShowGUI, $GUI_UNCHECKED)
					GUISetState(@SW_HIDE, $Form1)
				EndIf
			Case $idShowThumb
				TraySetState(8)
				TrayItemSetText($idShowThumb, "Show the image as thumbnail")
				GUISetState(@SW_SHOW, $Silent)
				Sleep(IniRead(@AppDataDir & "\IMGURViewer\Config.dat", "Param", "SilentThumbTime", 3000))
				GUISetState(@SW_HIDE, $Silent)
			Case $idDL
				TraySetState(8)
				Local $sFileName = StringTrimLeft(GUICtrlRead($Input3), StringInStr(GUICtrlRead($Input3), "/", $STR_NOCASESENSE, -1))
				If FileExists(@UserProfileDir & "\Pictures\Life_Of_IMGUR") <> 1 Then DirCreate(@UserProfileDir & "\Pictures\Life_Of_IMGUR")
				InetGet($sFileName, IniRead(@AppDataDir & "\IMGURViewer\Config.dat", "Param", "Savefolder", @MyDocumentsDir & "\Pictures\Life_Of_IMGUR"))
			Case $idShare
				TraySetState(8)
				ClipPut(GUICtrlRead($Input3))
				TrayTip("", "Link copied to clipboard!", 1, 16)
			Case $idOpen
				TraySetState(8)
				ShellExecute(GUICtrlRead($Input3))
				TrayItemSetText($idShowThumb, "Show the image as thumbnail")
		EndSwitch
	EndIf
	Switch $nMsg
		Case $GUI_EVENT_CLOSE
			If $Exit = 0 Then
				If _IsChecked($Checkbox2) Then
					IniWrite(@AppDataDir & "\IMGURViewer\Config.dat", "Param", "WhatsNew", 1)
					GUICtrlSetState($Checkbox3, 4)
				EndIf
				GUISetState(@SW_HIDE, $About)
				$Exit = 1
			Else
				$oIE3.quit()
				$oIE.quit()
				_IEQuit($oIE2)
				GUIDelete()
				Exit
			EndIf
		Case $btnCloseChangeLogGUI
			If _IsChecked($Checkbox2) Then
				IniWrite(@AppDataDir & "\IMGURViewer\Config.dat", "Param", "WhatsNew", 1)
				GUICtrlSetState($Checkbox3, 4)
			EndIf
			$Exit = 1
			GUISetState(@SW_HIDE, $About)
		Case $Random
			RandomURL()
		Case $Checkbox1
			If _IsChecked($Checkbox1) Then
				$checked = 1
				$i = 1
				While ($checked)
					For $i = 1 To ConfigRead("Timer") *50 Step 1
						$nMsg = GUIGetMsg()
						Switch $nMsg
							Case $GUI_EVENT_CLOSE
								Exit
							Case $Random
								RandomURL()
								$i = 1
							Case $Checkbox1
								If _IsChecked($Checkbox1) = 0 Then
									$checked = 0
								EndIf
							Case $btnCpLink1MainGUI
								ClipPut(GUICtrlRead($Input3))
							Case $btnCpLink2MainGUI
								ClipPut(GUICtrlRead($Input1))
							Case $btnCpLink3MainGUI
								ClipPut(GUICtrlRead($Input2))
							Case $btnDlMainGUI
								Local $pause = 1
								While $pause
									$i = $i
									Local $sFileName = StringTrimLeft(GUICtrlRead($Input3), StringInStr(GUICtrlRead($Input3), "/", $STR_NOCASESENSE, -1))
									Local $Savedir = FileSaveDialog("Save your pics...", ConfigRead("Savepics"), "Image (*.jpg)", 2, $sFileName)
									InetGet(GUICtrlRead($Input3), $Savedir)
									$pause = 0
								WEnd
						EndSwitch
						Sleep(10)
					Next
					RandomURL()
					$i = 1
				WEnd
			EndIf
		Case $btnCpLink1MainGUI
			ClipPut(GUICtrlRead($Input3))
		Case $btnCpLink2MainGUI
			ClipPut(GUICtrlRead($Input1))
		Case $btnCpLink3MainGUI
			ClipPut(GUICtrlRead($Input2))
		Case $btnLoadIMGHistGUI
			Local $URLList1 = StringRight(GUICtrlRead($List1), 32)
			$oIE3.navigate($URLList1)
		Case $btnDlHistory
			;If( ERRORTHERE
			GUICtrlSetData($HiddenInput, StringTrimLeft(StringRight(GUICtrlRead($List1), 11), StringInStr(StringRight(GUICtrlRead($List1), 11), "/", $STR_NOCASESENSE, -1)))
			Local $sFileName = StringLeft(GUICtrlRead($HiddenInput), 9)
			Local $Savedir = FileSaveDialog("Save your pics...", ConfigRead("Savepics"), "Image (*.jpg)", 2, $sFileName)
			InetGet($sFileName, $Savedir)
		Case $btnFavHistGUI
			MsgBox(0, "Still in dev..", "Actually pretty useless and just save the link on Favorite.txt...")
			FileWriteLine("Favorite.txt", GUICtrlRead($List1))
		Case $btnBlacklistHistGUI
			MsgBox(0, "Coming soon...", "Not implemented yet!")
		Case $btnClearHistGUI
			Local $msgbox = MsgBox(256 + 32 + 4, "Delete historical...", "Do you really want to delete your historical?")
			If $msgbox = 6 Then
				FileDelete(@AppDataDir & "\IMGURViewer\History.txt")
				GUICtrlSetData($List1, "", "")
				MsgBox(64, "Deleted", "Historical succesfully deleted.")
			EndIf
		Case $btnDlMainGUI
			Local $sFileName = StringTrimLeft(GUICtrlRead($Input3), StringInStr(GUICtrlRead($Input3), "/", $STR_NOCASESENSE, -1))
			Local $Savedir = FileSaveDialog("Save your pics...", ConfigRead("Savepics"), "Image (*.jpg)", 2, $sFileName)
			InetGet(GUICtrlRead($Input3), $Savedir)
		Case $btnSelectFolderParamGUI
			Local $Foldersave = FileSelectFolder("Select a folder", IniRead(@AppDataDir & "\IMGURViewer\Config.dat", "Param", "Savefolder", @UserProfileDir & "\Pictures"))
			If @error Then
				MsgBox(0, "", "No folder was selected.")
				GUICtrlSetData($Input5, @UserProfileDir & "\Pictures")
			Else
				GUICtrlSetData($Input5, $Foldersave)
			EndIf
		Case $btnCpLinkHistGUI
			ClipPut(StringRight(GUICtrlRead($List1), 32))
		Case $Save
			ConfigSave()
			GUICtrlSetData($Save, "Configuration sucessfully saved!", "")
			Sleep(1500)
			GUICtrlSetData($Save, "Save configuration", "")
			GUICtrlSetData($Checkbox1, _MultiLang_GetText("autrand") & " " & ConfigRead("Timer") & " secondes.")
		Case $Default
			GUICtrlSetData($Input4, IniRead(@AppDataDir & "\IMGURViewer\Config.dat", "Default", "DTimer", "10"))
			GUICtrlSetData($Input5, IniRead(@AppDataDir & "\IMGURViewer\Config.dat", "Default", "DSavefolder", @UserProfileDir & "\Pictures"))
			GUICtrlSetState($Checkbox3, 1)
			ConfigSave()
			GUICtrlSetData($Default, "Config resetted!", "")
			Sleep(1500)
			GUICtrlSetData($Default, "Back to default", "")
		Case $btnFavMainGUI
			MsgBox(0, "Still in dev..", "Actually pretty useless and just save the link on Favorite.txt...")
			FileWriteLine("Favorite.txt", GUICtrlRead($Input3))
		Case $btnBlackListMainGUI
			MsgBox(0, "Coming soon...", "Not implemented yet!")
		Case $btnOpenInBrowserMainGUI
			ShellExecute(GUICtrlRead($Input3))
		Case $btnUpdateParamGUI
			$Buttonpressed = 1
			Update()
			;			MsgBox(0,'',"No folks no more update")
	EndSwitch
WEnd

Func ConfigRead($param)
	If $param = "Timer" Then
		$TimeSec = IniRead(@AppDataDir & "\IMGURViewer\Config.dat", "Param", "Timer", "10")
		Return $TimeSec
	EndIf
	If $param = "Savepics" Then
		$Folderdir = IniRead(@AppDataDir & "\IMGURViewer\Config.dat", "Param", "Savefolder", @UserProfileDir & "\Pictures")
		Return $Folderdir
	EndIf
EndFunc   ;==>ConfigRead

Func ConfigSave()
	Local $Timer = IniRead(@AppDataDir & "\IMGURViewer\Config.dat", "Param", "Timer", "10")
	Local $Savefolder = IniRead(@AppDataDir & "\IMGURViewer\Config.dat", "Param", "Savefolder", @UserProfileDir & "\Pictures")
	Local $Language = IniRead(@AppDataDir & "\IMGURViewer\Config.dat", "Param", "Language", "0409")
	If $Language = "0409" Then $Language = $LANGFILES[0][0]
	If $Language = "040c" Then $Language = $LANGFILES[1][0]
	If $Timer <> GUICtrlRead($Input4) Then
		IniWrite(@AppDataDir & "\IMGURViewer\Config.dat", "Param", "Timer", GUICtrlRead($Input4))
	EndIf
	If $Savefolder <> GUICtrlRead($Input5) Then
		IniWrite(@AppDataDir & "\IMGURViewer\Config.dat", "Param", "Savefolder", GUICtrlRead($Input5))
	EndIf
	If _IsChecked($Checkbox3) Then
		IniWrite(@AppDataDir & "\IMGURViewer\Config.dat", "Param", "WhatsNew", 0)
	Else
		IniWrite(@AppDataDir & "\IMGURViewer\Config.dat", "Param", "WhatsNew", 1)
	EndIf
	If _IsChecked($Checkbox4) Then
		IniWrite(@AppDataDir & "\IMGURViewer\Config.dat", "Param", "SilentMode", 1)
		GUICtrlSetState($Input6, $GUI_ENABLE)
		TraySetState(1)
		Opt("TrayMenuMode", 3)
		GUISetState(@SW_HIDE, $Form1)

		Global $Silent = GUICreate("", 325, 325, 351, 174, 0)
		GUISetState(@SW_HIDE)
		$sIE = ObjCreate("Shell.Explorer.2")
		$sIEActiveX = GUICtrlCreateObj($sIE, -1, -1, 325, 325)
	Else
		IniWrite(@AppDataDir & "\IMGURViewer\Config.dat", "Param", "SilentMode", 0)
		GUICtrlSetState($Input6, $GUI_DISABLE)
		TraySetState(2)
	EndIf
	If GUICtrlRead($Input6) > 10 Then
		MsgBox(0, "ERROR!", "Time for thumb view cannot exceed 10 (ten) seconds.")
	ElseIf GUICtrlRead($Input6) <= 0 Then
		MsgBox(0, "ERROR!", "Time for thumb view cannot equal or under 0 seconds. Sucka.")
	Else
		IniWrite(@AppDataDir & "\IMGURViewer\Config.dat", "Param", "SilentThumbTime", GUICtrlRead($Input6) * 1000)
	EndIf
	If $Language <> GUICtrlRead($Combo) Then
		Local $LangCode
		If GUICtrlRead($Combo) == $LANGFILES[0][0] Then
			$LangCode = $LANGFILES[0][2]
			IniWrite(@AppDataDir & "\IMGURViewer\Config.dat", "Param", "Language", $LangCode)
			MsgBox(0, "Rebooting...", "The application will restart to change the languages.")
			ShellExecute(@ScriptName)
			Exit
		ElseIf GUICtrlRead($Combo) == $LANGFILES[1][0] Then
			$LangCode = $LANGFILES[1][2]
			IniWrite(@AppDataDir & "\IMGURViewer\Config.dat", "Param", "Language", $LangCode)
			MsgBox(0, "Red�marrage...", "L'application va red�marrer pour pouvoir changer la langue.")
			ShellExecute(@ScriptName)
			Exit
		EndIf
	EndIf
EndFunc   ;==>ConfigSave

Func RandomURL()
	GUICtrlSetData($Progress1, 0)
	GUICtrlSetData($List1, "", "")
	Local $URL[10]
	Local $randomlettersornumber
	Global $RandomedURL = "https://i.imgur.com/"
	For $i = 1 To 5 Step 1 ;1 to 6 work but it's so SLOOOOOOW
		$randomlettersornumber = Random(1, 3, 1)
		If $randomlettersornumber = 1 Then
			$URL[$i] = Chr(Random(Asc("a"), Asc("z"), 1))
		ElseIf $randomlettersornumber = 2 Then
			$URL[$i] = Random(0, 9, 1)
		Else
			$URL[$i] = Chr(Random(Asc("A"), Asc("Z"), 1))
		EndIf
		$RandomedURL &= $URL[$i]
	Next
	$RandomedURL = $RandomedURL & ".png"
	ConsoleWrite("$RandomedURL : " & $RandomedURL & @CRLF)
	_IENavigate($oIE2, $RandomedURL, 1)
	Local $sLocationUrl
	$sLocationUrl = _IEPropertyGet($oIE2, "locationurl")
	ConsoleWrite("$sLocationUrl : " & $sLocationUrl & @CRLF)
;~ 	If StringLen($RandomedURL) == 29 Then
	If $sLocationUrl == "https://i.imgur.com/removed.png" Then
		Return RandomURL()
	EndIf
;~ 	EndIf
;~ 	If StringLen($RandomedURL) > 29 Then
;~ 		Local $sTitle
;~ 		$sTitle = _IEPropertyGet($oIE2, "title")
;~ 		ConsoleWrite("$sTitle : " & $sTitle & @CRLF)
;~ 		If $sTitle == "imgur: the simple 404 page" Then
;~ 			Return RandomURL()
;~ 		EndIf
;~ 	EndIf
	If IniRead(@AppDataDir & "\IMGURViewer\Config.dat", "Param", "SilentMode", 0) = 1 Then
		$SilentLink = StringTrimRight($RandomedURL, 4) & "m.jpg"
		$sIE.navigate($SilentLink)
		TrayItemSetText($idShowThumb, "(1) Show the image as thumbnail")
	EndIf
	$oIE.navigate($RandomedURL)
	GUICtrlSetData($Input3, $RandomedURL, "")
	FileWrite(@AppDataDir & "\IMGURViewer\History.txt", @MDAY & "/" & @MON & "/" & @YEAR & " " & @HOUR & ":" & @MIN & ":" & @SEC & "         " & GUICtrlRead($Input3))
	FileWrite(@AppDataDir & "\IMGURViewer\History.txt", @CR)
	FileWriteLine(@AppDataDir & "\IMGURViewer\History.txt", "|")
	GUICtrlSetData($Input2, "[URL=" & $RandomedURL & "][IMG]" & $RandomedURL & "[/IMG][/URL]", "")
	GUICtrlSetData($Input1, "[IMG]" & $RandomedURL & "[/IMG]", "")
	GUICtrlSetData($Progress1, 100)
	GUICtrlSetData($List1, FileRead(@AppDataDir & "\IMGURViewer\History.txt"), "")
EndFunc   ;==>RandomURL

Func _IsChecked($iControlID)
	Return BitAND(GUICtrlRead($iControlID), $GUI_CHECKED) = $GUI_CHECKED
EndFunc   ;==>_IsChecked

Func Update()
	$IniUpdate = InetGet($ServerURL & "update", @TempDir & "\update")
	If $IniUpdate = 0 Then
		MsgBox(0, "ERROR", _MultiLang_GetText("updatefunc1"), 3)
		Exit 1
	EndIf
	Local $NewVersion = IniRead(@TempDir & "\update", "Version", "Version", "-1")
;~ 	MsgBox(0,"",$NewVersion)
	If $VersionNumber = "DevRelease" Then
		MsgBox(0, "Info...", "You're currently using a dev release, this can mean two things: You're me (ShiiroSan) or you found a bug.")
	Else
		If $NewVersion <> $VersionNumber Then
			Local $msg = MsgBox(4, "Update", _MultiLang_GetText("updatefunc2") & $NewVersion & _MultiLang_GetText("updatefunc21") & $VersionNumber & _MultiLang_GetText("updatefunc22"))
;~ 		If $msg = 7 Then ;No was pressed
;~ 			Exit
			If $msg = 6 Then ;OK was pressed
				Local $LangChoose = IniRead(@AppDataDir & "\IMGURViewer\Config.dat", "Param", "Language", "090c")
				If $LangChoose = "0904" Then $LangChoose = "ENGLISH"
				If $LangChoose = "090c" Then $LangChoose = "FRENCH"
				$downloadLink = $ServerURL & "newversion.exe"
				$dlhandle = InetGet($downloadLink, @WorkingDir & "\Imgur Image Viewer v" & $NewVersion & ".exe", 1, 1)
				InetGet($ServerURL & $LangChoose, @AppDataDir & "\IMGURViewer\LanguagesFiles\" & $LangChoose & ".EUX", 1, 1)
				ProgressOn("", "", "", -1, -1, 16) ;creates an progressbar
				$Size = InetGetSize($downloadLink, 1) ;get the size of the update
				While Not InetGetInfo($dlhandle, 2)
					$Percent = (InetGetInfo($dlhandle, 0) / $Size) * 100
					ProgressSet($Percent, $Percent & " percent") ;update progressbar
				WEnd
				ProgressSet(100, "Done", "Complete") ;show complete progressbar
				Sleep(500)
				ProgressOff() ;close progress window
				IniWrite(@AppDataDir & "\IMGURViewer\Config.dat", "Version", "Actual", $NewVersion) ;updates update.ini with the new version
				InetClose($dlhandle)
				MsgBox(-1, "Success", _MultiLang_GetText("updatefunc3"))
				ShellExecute("Imgur Image Viewer v" & $NewVersion & ".exe")
				FileDelete(@TempDir & "\update")
				_SelfDeleteBatch(2, 0)
			EndIf
		Else
			If $Buttonpressed = 1 Then MsgBox(0, "No update!", _MultiLang_GetText("updatefunc4"))
		EndIf
	EndIf
	FileDelete(@TempDir & "\update")
	$Buttonpressed = 0
EndFunc   ;==>Update

Func _StringBetween2($s, $from, $to)
	$x = StringInStr($s, $from) + StringLen($from)
	$y = StringInStr(StringTrimLeft($s, $x), $to)
	Return StringMid($s, $x, $y)
EndFunc   ;==>_StringBetween2
